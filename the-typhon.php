<?php require_once('partials/header.html');?>

    <!-- preloader -->
    <div id="preloader">
        <div class="pre-container">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <!-- end preloader -->

    <div class="container-fluid">
    
    	<!-- prey header -->
    	<div class="prey-header">
    		<a href="https://prey.bethesda.net" target="_blank"><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
        	<a href="https://prey.bethesda.net/preorder/" target="_blank"><img class="prey-preorder" src="img/prey-preorder.png" alt="Pre-order"></a>
        	<a href="https://prey.bethesda.net/preorder/" target="_blank"><img class="prey-boxshots" src="img/prey-boxshots.png" alt="Boxshots"></a>
        	<img class="prey-coming" src="img/prey-coming.png" alt="Coming 5.5.17">
    	</div>
    
 		<!-- transtar header -->
        <header class="box-header">
            <div class="box-logo">
                <a href="index.php"><img src="img/transtar-logo.png" width="195" alt="TranStar"></a>
            </div>
            
            <div class="date">
        		<div id="time"></div>
			</div>
            
            <div class="location">
        		TRANSTAR HQ<div id="weather"></div>
			</div>
            
            <!-- nav button -->
            <a class="box-primary-nav-trigger" href="#0">
                <span class="box-menu-text">MENU</span><span class="box-menu-icon"></span>
            </a>
        </header>
        <!-- end header -->

        <?php require_once('partials/nav.html'); ?>
    
    	<!-- header -->
    	<div class="top-bar">
        	<div class="article1-top-bar-text">
        	<h1>THE NEW YOU</h1>
        	<p>LIVE BETTER THROUGH TRANSTAR</p>
            </div>
    	</div>
    	<!-- end header -->
	</div>
    
    <div>
        <!-- content -->
        <div class="container main-container clearfix article"> 
            <div class="col-md-12">
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla commodo interdum mauris non cursus. Proin ac mauris ornare, mollis augue eu, porta diam. Ut sed arcu turpis. Suspendisse tempor nulla in nisl pulvinar auctor.</p> 
                <p>Morbi aliquam erat quis ligula interdum venenatis. Etiam lacus tellus, elementum id tempor nec, consequat sed velit. Nulla mauris felis, aliquam suscipit pharetra sit amet, aliquam eu lorem. Suspendisse volutpat ultricies ullamcorper.</p>
            </div>
            <div class="col-md-12">
                <img src="img/article-01.jpg" alt="Join Transtar" class="img-responsive" />
            </div>
            <div class="col-md-12">
                <p>Aenean lacinia ante sem. Etiam vulputate lorem ut quam faucibus vulputate. Quisque at tempus quam. Donec bibendum porttitor neque, nec ultricies magna tempor nec. Sed et lorem ex. Suspendisse vitae ultricies massa. Sed dolor ante, porttitor vitae pretium ut, faucibus id diam. In hac habitasse platea dictumst.</p>
                <p>Integer porta dolor eget luctus porttitor. Nulla nibh justo, dignissim quis rutrum sed, venenatis sit amet nulla. Aenean augue lorem, venenatis sed felis in, posuere ullamcorper felis. Suspendisse potenti. Ut efficitur purus ex, ac aliquam tellus fermentum vitae. Morbi posuere imperdiet lectus, quis posuere elit tempor id. Donec id eros magna. Duis tristique lectus eget urna consequat varius.</p>
                <p>Nulla vulputate congue sollicitudin. Vivamus vehicula orci placerat ligula dignissim bibendum. Sed vel facilisis elit. Sed orci odio, interdum eu augue sed, lacinia porttitor diam. Vivamus tempor dolor eu augue elementum, quis accumsan nisi scelerisque. Integer id scelerisque augue. Donec egestas facilisis libero, imperdiet facilisis urna blandit eu.</p>
            </div>
            <div class="col-md-12">
                <h3 class="bold">LOREM IPSUM DOLOR</h3>
            </div>
            <div class="col-md-12">
                <p>In eget augue porttitor, ultricies turpis id, sodales ante. Donec hendrerit aliquet sapien id tincidunt. Nulla vitae magna ac turpis feugiat vulputate quis sit amet felis. In id risus neque. In efficitur nunc eget ipsum suscipit, sit amet bibendum enim ornare. Pellentesque tempor at odio at facilisis. Sed molestie massa id cursus ultrices. Donec tempus nulla orci, non venenatis libero euismod sit amet. Nulla diam mauris, efficitur eu dictum in, vestibulum et diam. Proin condimentum scelerisque purus a porta. Quisque laoreet faucibus massa vitae semper. </p>
                <p>Duis posuere, ligula id dignissim bibendum, leo arcu pretium sem, ac gravida dui libero ut libero. Aliquam vitae tellus ut neque bibendum fermentum. Duis et elit commodo, luctus massa vel, auctor dolor. Ut odio ipsum, aliquam tempus nulla ut, posuere ultricies dui. </p>
            </div>
            <div class="col-md-12">
                <img src="img/article-02.jpg" alt="Join Transtar" class="img-responsive" />
            </div>
            <div class="col-md-12">
                <p>Sed eu congue dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas vel pellentesque risus. Cras in dolor justo. Sed sed volutpat massa, in tempor nulla. Morbi magna urna, malesuada ac ligula varius, bibendum scelerisque lacus.</p>
                <p>Curabitur ut massa mi. Etiam lobortis ac metus quis elementum. Nam convallis, mauris quis mollis efficitur, nibh eros cursus purus, sit amet egestas quam quam vel lacus. Fusce luctus purus quis sem sollicitudin bibendum. Aliquam sed tincidunt lorem. Vestibulum eget diam purus. </p>
                <p>Curabitur odio libero, lobortis quis sodales eu, faucibus sit amet velit. Ut tempor tellus sit amet lectus porttitor bibendum. Mauris convallis risus quam, id malesuada est varius id. Vivamus laoreet id tellus et suscipit. Nullam pellentesque felis quis tempor tincidunt. Nam ac odio ac est imperdiet interdum ultrices eu diam. Aenean nec odio quis neque sollicitudin convallis. Aliquam euismod turpis at commodo cursus. Aenean orci massa, sollicitudin in arcu ut, laoreet pharetra elit. Donec at arcu vitae elit fringilla egestas eget in eros.</p>
            </div>
        </div>
        
        <!-- transtar footer -->
        <div class="transtar-footer">
            <div class="copyright">
                <img src="img/dashbar.png" alt=""><br>OFFICE OF INTERNAL COMMUNICATIONS<br>TRANSTAR CORPORATION
            </div>
            <ul>
                <li class="social"><a href="https://www.facebook.com/prey" target="_blank"><i class="ion-social-facebook"></i></a></li>
                <li class="social"><a href="https://twitter.com/preygame" target="_blank"><i class="ion-social-twitter"></i></a></li>
                <li class="social"><a href="https://www.instagram.com/preygame" target="_blank"><i class="ion-social-instagram-outline"></i></a></li>
            </ul>
        </div>
        
        <!-- prey footer -->
        <div class="prey-footer">
            <a href="https://prey.bethesda.net" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Link']);" ><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
            <a href="https://bethesda.net/en/dashboard" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Dashboard Link']);" ><img class="bethesda-logo" src="img/bethesda-logo.png" alt="Pre-order"></a>
            <a href="http://www.arkane-studios.com/uk/index.php" onclick="_gaq.push(['_trackEvent', 'Footer - Arkane Studios Link']);"  target="_blank"><img class="arkane-logo" src="img/arkane-logo.png" alt="Boxshots"></a>
        </div>
        
    </div>
    <!-- end container -->

    <footer class="ziffdavis-footer">
      <a href="http://www.ziffdavis.com" target="_blank"><img src="img/zd_logo.png" alt="Ziff Davis"></a>
      <div class="text-center">
        <p>&#169; <script type="text/javascript"> document.write(new Date().getFullYear()); </script> Ziff Davis International Ltd</p>
        <ul>
          <li><a href="http://uk.corp.ign.com/#about" target="_blank" title="About Us">About Us</a></li>
          <li><a href="http://uk.corp.ign.com/#contact" target="_blank" title="Contact Us">Contact Us</a></li>
          <li><a href="http://corp.ign.com/feeds.html" target="_blank" title="RSS Feeds">RSS Feeds</a></li>
          <li><a href="http://corp.ign.com/privacy.html" target="_blank" title="Privacy Policy">Privacy Policy</a></li>
          <li><a href="http://corp.ign.com/user-agreement.html" target="_blank" title="Terms of Use">Terms of Use</a></li>
        </ul>
      </div>
    </footer>


    <!-- Back to top -->
	<a href="#0" class="cd-top"><i class="ion-android-arrow-up"></i></a>

<!-- Scripts -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>

<?php require_once('partials/footer.html');?>