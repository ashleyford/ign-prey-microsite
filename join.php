<?php require_once('partials/header.html');?>

    <!-- preloader -->
    <div id="preloader">
        <div class="pre-container">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <!-- end preloader -->

    <div class="container-fluid">
    
    	<!-- prey header -->
    	<div class="prey-header">
    		<a href="https://prey.bethesda.net" target="_blank"><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
        	<a href="https://prey.bethesda.net/preorder/" target="_blank"><img class="prey-preorder" src="img/prey-preorder.png" alt="Pre-order"></a>
        	<a href="https://prey.bethesda.net/preorder/" target="_blank"><img class="prey-boxshots" src="img/prey-boxshots.png" alt="Boxshots"></a>
        	<img class="prey-coming" src="img/prey-coming.png" alt="Coming 5.5.17">
    	</div>
    
 		<!-- transtar header -->
        <header class="box-header">
            <div class="box-logo">
                <a href="index.php"><img src="img/transtar-logo.png" width="195" alt="TranStar"></a>
            </div>
            
            <div class="date">
        		<div id="time"></div>
			</div>
            
            <div class="location">
        		TRANSTAR HQ<div id="weather"></div>
			</div>
            
            <!-- nav button -->
            <a class="box-primary-nav-trigger" href="#0">
                <span class="box-menu-text">MENU</span><span class="box-menu-icon"></span>
            </a>
        </header>
        <!-- end header -->

        <?php require_once('partials/nav.html'); ?>

        <!-- gallery -->
        <a href="img/gallery/2.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
        <a href="img/gallery/3.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
        <a href="img/gallery/4.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
        <a href="img/gallery/5.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
        <a href="img/gallery/6.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
        <a href="img/gallery/8.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
        <!-- end -->
    
    	<!-- header -->
    	<div class="top-bar">
        	<div class="top-bar-text">
        	<h1>JOIN TRANSTAR TODAY</h1>
        	<p>CONTINUE YOUR TRANSTAR RECRUITMENT JOURNEY BY TAKING THE PSYCHOMETRIC TEST</p>
            </div>
    	</div>
    	<!-- end header -->
	</div>
    
    <!-- content -->
    <div class="container main-container clearfix join-intro"> 
        <div class="col-md-6">
            <img src="img/join.jpg" alt="Join TranStar" class="img-responsive" />
        </div>
        <div class="col-md-6">
           <h2>Welcome to the final stage of the Transtar recruitment process. </h2>
           <p>Soon, you’ll be on your way to the cutting edge Talos I innovation centre but first you are required to complete a simple psychometric test. There are no right or wrong responses to these questions. Instead, this test is intended to help our recruitment analysts match your skills, temperament and personality to the positions available at Transtar’s Talos I facility. </p>
           <p>We currently have positions available in the following areas:</p>
           <p>- Science Division</p>
           <p>- Engineering Sector</p>
          <p>- Security Detail </p>
          <p>The eight questions that follow have been randomly selected from a pool of hundreds. Therefore, the subject matter may be hypothetical in nature and does not necessarily reflect the work undertaken aboard Talos I by Transtar corporation or its employees.</p>
           <p>Please proceed, we look forward to observing your responses.</p>
          <a href="#test" onclick="_gaq.push(['_trackEvent', 'Join - Workstation Login']);" ><button type="button" class="btn btn-style">TAKE TEST</button></a>
		</div>
  	</div>
    
    <!-- workstation -->  
    <div id="test"> 
        <div class="toolbar">
    		<div class="container">
            	<p><span class="gold">CURRENT USER:</span> ANONYMOUS</p>
            </div>
    	</div>
        
        <div class="container"> 
        	<div class="test-header">
    			<img class="workstation-logo" src="img/workstation-logo.png" alt="Workstation">
        		<div class="test-header-text">
                	<h2>PSYCHOMETRIC TEST</h2>
                    <img class="transtar-logo-test" src="img/transtar-logo-test.png" alt="TranStar">
                </div>
    		</div>
     		
            <!-- test -->
        	<div class="col-md-12">
            	<div id="quizzie">
            	
                	<div id="quiz-test">
                		<!-- question 1 -->
    					<ul class="quiz-step quiz step1 current" data-question="1">
        					<li class="question">
           					  <div class="question-wrap">
           					    <p>Question 1</p>
            					  <p>Which of the following statements, paraphrasing President John F Kennedy, best sums up why you want to work for Transtar aboard the Talos I innovation centre? </p>
            					</div>
        					</li>
        					<li class="quiz-answer low-value" data-quizindex="0">
            					<div class="answer-wrap"> 
                					<div class="paragraph"><p class="answer-text">I’m interested in working with the most extraordinary collection of talent and human knowledge that has ever been gathered.</p></div>
                					<div class="circle"></div>
            					</div>
        					</li>
        					<li class="quiz-answer" data-quizindex="1">
            					<div class="answer-wrap"> 
                					<div class="paragraph"><p class="answer-text">I refuse to settle for second place. Once you say you’re going to settle for second that’s what happens to you in life. </p></div>
                					<div class="circle"></div>
            					</div>
       						</li>
        					<li class="quiz-answer high-value" data-quizindex="2">
            					<div class="answer-wrap"> 
                					<div class="paragraph"><p class="answer-text">The pay is good and I can walk to work. </p></div>
                					<div class="circle"></div>
            					</div>
        					</li>
                    		<div class="number">1/8</div>
    					</ul>
      
      					<!-- question 2 -->          
                        <ul class="quiz-step quiz step2" data-question="2">
                            <li class="question">
                                <div class="question-wrap">
                                    <p>Question 2</p>
                                    <p>You have been working at a large, well-respected research & development corporation for several months before you begin to hear rumours that the company is pursuing psychotronic research via questionable practices. What is your response?</p>
                                 </div>
                            </li>
                            <li class="quiz-answer low-value" data-quizindex="2">
                                <div class="answer-wrap"> 
                                    <div class="paragraph"><p class="answer-text">Subtly question colleagues in order to verify the source of the rumour and then take appropriate measures to deal with the person responsible for the leaked information. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer" data-quizindex="0">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">Attempt to learn more about the research in question to see if I can contribute to or benefit from the results. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer high-value" data-quizindex="1">
                                <div class="answer-wrap"> 
                                    <div class="paragraph"><p class="answer-text">It’s how the biggest, most profound advancements are made and “questionable practices” are not necessarily illegal. It’s none of my concern.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <div class="number">2/8</div>
                        </ul>
                        
                        <!-- question 3 -->
                        <ul class="quiz-step quiz step3" data-question="3">
                            <li class="question">
                                <div class="question-wrap">
                                    <p>Question 3</p>
                                    <p>Neuromods offer the ability to enhance human abilities. Which of these Transtar developed neuromods would you be most interested in?</p>
                                </div>
                            </li>
                            <li class="quiz-answer low-value" data-quizindex="0">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">The ability to modify my genetic make-up, increase my perception and heighten my decision making powers. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer" data-quizindex="1">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">The capability to gain a mastery of tools, circuits and mechanical devices, and enhance my ability to modify, adapt, transform and repurpose the things around me.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer high-value" data-quizindex="2">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">I don’t want to have to rely on external factors. To be better, move faster, feel stronger and hit harder, is my kind of enhancement. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <div class="number">3/8</div>
                        </ul>
                        
                        <!-- question 4 -->
                        <ul class="quiz-step quiz step4" data-question="4">
                            <li class="question">
                                <div class="question-wrap">
                                    <p>Question 4</p>
                                    <p>What do you believe to be the most impressive part of Talos I?</p>
                                </div>
                            </li>
                            <li class="quiz-answer low-value" data-quizindex="1">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">The Neuromod Division – Transtar’s Skill Recorders and neuromod applicators are mind blowing. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer" data-quizindex="2">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">The Station itself– I want to get a feel for the place as a whole, followed by a visit to the cafeteria, I hear the cakes they sell are out of this world.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer high-value" data-quizindex="0">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">The Psychotronics Lab – Its work is helping expand the boundaries of human limitations. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <div class="number">4/8</div>
                        </ul>
                        
                        <!-- question 5 -->
                        <ul class="quiz-step quiz step5" data-question="6">
                            <li class="question">
                                <div class="question-wrap">
                                    <p>Question 5</p>
                                    <p>What would you consider the biggest downside of an expensive, high-end consumer product that remaps the brain’s synaptic pathways to enhance abilities by way of a visual organ delivery system (for example, a large needle injected directly into the user’s eyeball)?</p>
                                </div>
                            </li>
                            <li class="quiz-answer low-value" data-quizindex="0">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">The potential long term implications on the brain’s physiology.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer" data-quizindex="1">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">It would make rich, lazy people, even richer and lazier.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer high-value" data-quizindex="2">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">“A large needle injected directly into the user’s eyeball”. Yeah. That one. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <div class="number">5/8</div>
                        </ul>
                        
                        <!-- question 6 -->          
                        <ul class="quiz-step quiz step6" data-question="6">
                            <li class="question">
                                <div class="question-wrap">
                                    <p>Question 6</p>
                                    <p>Which of these quotes do you most closely identify with?</p>
                                 </div>
                            </li>
                            <li class="quiz-answer low-value" data-quizindex="2">
                                <div class="answer-wrap"> 
                                    <div class="paragraph"><p class="answer-text">“Forgive your enemies but never forget their names”. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer" data-quizindex="0">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">“The human mind is our fundamental resource”.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer high-value" data-quizindex="1">
                                <div class="answer-wrap"> 
                                    <div class="paragraph"><p class="answer-text">“Change is the law of life”.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <div class="number">6/8</div>
                        </ul>
                        
                        <!-- question 7 -->
                        <ul class="quiz-step quiz step7" data-question="7">
                            <li class="question">
                                <div class="question-wrap">
                                    <p>Question 7</p>
                                    <p>Which of these everyday devices found aboard Talos I most appeals to you?</p>
                                </div>
                            </li>
                            <li class="quiz-answer low-value" data-quizindex="1">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">Fabricator – I like to tinker and craft blueprints. Seeing those plans become fully-usable objects never gets old. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer" data-quizindex="0">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">Recycler – I have strange habit of picking up every bit of junk I find, so being able to turn that trash into useful materials and components is excellent news.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer high-value" data-quizindex="2">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">Huntress Boltcaster – It fires foam bolts at colleagues from across the room, which means I’ll never get bored, and I can open and close doors without getting out of my seat. Genius.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <div class="number">7/8</div>
                        </ul>
                        
                        <!-- question 8 -->
                        <ul class="quiz-step quiz step8" data-question="8">
                            <li class="question">
                                <div class="question-wrap">
                                    <p>Question 8</p>
                                    <p>Which of these non-terrestrial life-forms would you judge to be the most “interesting”?</p>
                                </div>
                            </li>
                            <li class="quiz-answer low-value" data-quizindex="2">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">One capable of shape-shifting to mimic inanimate everyday objects; the perfect scout and spy.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer" data-quizindex="0">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">One capable of telepathic communication and force projection; both dangerous, but potentially useful, abilities.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer high-value" data-quizindex="1">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">One capable of weaving other life-forms from the manipulation of raw energy; adaptable and crucial to an organism’s survival and fascinating to behold. </p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <div class="number">8/8</div>
                        </ul>

                        <!-- tie break -->
                        <ul class="quiz-step tie-break step9" data-question="9">
                            <li class="question">
                                <div class="question-wrap">
                                    <p>Bonus Question</p>
                                    <p>A system error has led to you being locked out of your office, what action do you take?</p>
                                </div>
                            </li>
                            <li class="quiz-answer low-value" data-quizindex="0">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">Hack the lock; it'll be quicker than waiting for IT to rectify the issue.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer" data-quizindex="1">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">Recalibrate your suit modifications so you can pry open the door; simple but effective.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <li class="quiz-answer high-value" data-quizindex="2">
                                <div class="answer-wrap">
                                    <div class="paragraph"><p class="answer-text">Consult the blueprints to find an alternative way into your office; nobody else needs to know.</p></div>
                                    <div class="circle"></div>
                                </div>
                            </li>
                            <div class="number"></div>
                        </ul>

    			
                        <!-- results -->
                        <ul id="results">
                            <li class="results-inner">
                                <div class="congrats">
                                	<p>Congratulations, you have been selected to be</p>
                                	<div id="result"><h1></h1></div>
                                	<p class="desc"></p>
                                	<div class="paragraph"><p class="answer-text">We look forward to welcoming you aboard. See you on Talos I!</p></div>
                    
                                	<button type="button" class="btn btn-style tweet" onclick="return tweetIt()">TWEET</button>
                                	<button type="button" id="shareBtn" class="btn btn-style share" onclick="shareBtn">SHARE</button>
                    
                                	<p><a href="javascript:window.location.reload(true);" onclick="_gaq.push(['_trackEvent', 'Join - Take Test Again Link']);" >TAKE THE TEST AGAIN</a></p>
                                	<div class="h-30"></div>
                   				 </div>
                                 
                                <!-- competition -->
                                <div class="competition">
                                    <div class="container-fluid">
              							<h2>Win Prey, a PS4 Pro and a 4K TV!</h2>
                                        <p>We'll be sending one lucky winner some great Prey merch, a copy of the game a PS4 Pro and a 4K TV  to play it on! All you need to do to be in with a chance of winning is fill in your details below and hit 'submit'!</p>
                                        <p>Please note this competition closes at 10am on 08 May 2017 and is open only to UK residents aged 18 or over.</p>
                                        <div class="h-20"></div>
                                    
                                        <div class="col-md-6">
                                            <img src="img/prize-final.jpg?v=2.0" class="img-responsive" alt="Prize" />
                                        </div>         
                                        
                                        <div class="col-md-6">

                                        <div id="response_holder"></div>

                                            <form action="#" method="post" id="competition_form">
                                                <div class="row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="bold">FIRST NAME</label>
                                                        <input type="text" class="form-control" name="first_name">
                                                    </div>
                                                    <div class="h-10-hide"></div>
                                                    <div class="col-md-6 text-left">
                                                        <label class="bold">SURNAME</label>
                                                        <input type="text" class="form-control" name="last_name">
                                                    </div>
                                                </div>
                                                <div class="h-10"></div>
                                                <div class="row">
                                                    <div class="col-md-12 text-left">
                                                        <label class="bold">EMAIL ADDRESS</label>
                                                        <input type="email" class="form-control" name="email">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 text-left">
                                                        <fieldset class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="terms"><span>I confirm I am a UK resident aged 18 or over and have read and understood the <a data-toggle="modal" href="#terms" onclick="_gaq.push(['_trackEvent', 'Comp - Terms Link']);" >terms and conditions</a>.</span>
                                                                </label>
                                                            </div>
                                                        </fieldset>  
                                                        <input type="hidden" class="form-control" id="type" name="type" value="">
                                                        <button type="button" id="enter_comp" onclick="_gaq.push(['_trackEvent', 'Comp - Submit Btn']);"  class="btn btn-style">ENTER COMPETITION</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- end competition -->
                                
                            </li>
                        </ul>
                        <!-- end results -->
                        </div>
                    </div>
                </div>
                <!-- end quiz -->
            </div>
            <!-- end content -->
            
     	</div>
  
    
<!-- terms modal ----------------------------------------------------------------------------->
<div class="modal fade" id="terms" role="dialog">
<div class="modal-close"><a data-dismiss="modal"><img src="img/close.gif" alt="close"></a></div>
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">TERMS AND CONDITIONS</h4>
</div>
<div class="modal-body">
<p>
    1. By participating in the Prey Promotion (the "Promotion") promoted by
    Ziff Davis International Ltd. (@IGNUK) (the "Promoter"), you agree to these
    Official Terms and Conditions (the “Terms and Conditions”). When you enter
    the Promotion, these Terms and Conditions become a contract between you and
    the Promoter, so read them carefully before participating. This contract
    includes, without limitation, an indemnification by you of the released
    parties listed below and a limitation of your rights and remedies.
</p>
<p>
    <br/>
    2. The promoter of the Promotion is Ziff Davis International Ltd., 18
    Mansell Street, London, E1 8AA (the “Promoter”).
</p>
<p>
    3. Entry is open to users who are legally resident in the United Kingdom
    and are eighteen (18) years of age or over. Entrants who are eligible to
    enter the Promotion pursuant to these Terms and Conditions are referred to
    as “Eligible Entrants”.
</p>
<p>
    <br/>
    4. Directors, management, employees and officers of the Promoter and its
    related corporate entities (such as parent and subsidiary companies), as
    well as employees of associated agencies involved in the Promotion and
    their immediate families are ineligible to enter this Promotion.
</p>
<p>
    5. The Promotion commences 10am (BST) on 11 April, 2017, and closes at 10am
    (BST) on 08 May, 2017 (the "Promotion Period”).
</p>
<p>
    6. To enter the Promotion, entrants must go to

To enter the Promotion, entrants must go to     <a href="http://uk-microsites.ign.com/prey/join.php">
        <strong>http://uk-microsites.ign.com/prey/join.php</strong>
        
    </a>, fill out the quiz, and enter their personal details. Subsequent attempts made by the same individual to submit multiple entries by using multiple accounts or otherwise may result in disqualification of the entrant.
</p>
<p>
    Entries submitted by any other means other than by filling out the
    specified survey will not be accepted. Any illegible, incomplete or
    fraudulent entries will be rejected. Only one entry per person will be
    accepted. Use of script, macro or any automated system to enter the
    Promotion is prohibited and entries made (or which appear to have been
    made, in Promotor’s sole discretion) using any such system may be treated
    as void. All entries become the property of the Promoter. Participants must
    follow the directions provided by the Promoter and otherwise be bound by
    and follow these Terms and Conditions to be eligible to receive a Prize in
    connection with the Promotion.
    <strong>
        Participants may not take actions that are negligent, injurious or
        reckless in pursuit of any prize offered in connection with the
        Promotion. Participants who do not follow this rule or who otherwise
        participate in the Promotion in an unsportsmanlike manner will be
        disqualified from the Promotion. Participants alone are responsible for
        any injuries or other damages that are caused or incurred as a result
        of their behavior.
    </strong>
</p>
<p>
    7. Promoter will determine the giveaway recipients in its sole and absolute
    discretion in accordance with these Terms and Conditions. Each Prize
    recipient will be selected and contacted by Promoter. If the Promotion
    prize recipient is determined to be ineligible to receive the Prize,
    Promoter may, in its sole and absolute discretion, not give away the goods
    or may give the goods to the next Eligible Entrant. If you are selected as
    a prize recipient, you will be required to privately (by email or other
    secure, private means) provide your name, address, age and other personal
    information necessary to receive the Prize. The name, contact number and
    address of each prize recipient will be passed to a third party provider
    for delivery. Except for Winner Information (defined below), Promoter will
    not keep or store your information or use it for any purpose other than
    sending the Prize or providing you information on receiving the Prize.
    Promotions are subject to all applicable laws, rules or regulations and
    these Terms and Conditions.
</p>
<p>
    8. The winning entries will be drawn at random on 07 May, 2017 from all
    complete, eligible, properly submitted entries received by Promoter during
    the Promotion Period. The first entrant whose entry is drawn from the
    eligible entries shall win the Prize described below. There will be only
    one prize recipient for this Promotion. The Prize Winner will be notified
    by email on or around 07 May, 2017. If a Prize Winner does not reply to the
    notification email acknowledging acceptance of the Prize within five (5)
    working days, such Prize Winner will forfeit the Prize. In such instance,
    the Promoter reserves the right to award the Prize to another Eligible
    Entrant.
</p>
<p>
    9. There will be one (1) winner of the Promotion. The winning entrant (the “Prize Winner”) will receive the following prize (the “Prize”): </p>

    <p>One (1) Sony Bravia KD49XD8088 49-inch Android 4K HDR Ultra HD Smart TV, with an estimated retail value of six hundred and forty nine pounds sterling (£649);</p>
    <p>One (1) PlayStation 4 Pro 1TB, with an estimated retail value of three hundred and forty nine pounds sterling (£349);</p>
    <p>One (1) copy of Prey on PlayStation 4 at launch (after 5 May, 2017), with an estimated retail value of forty four pounds sterling (£44);</p>
    <p>One (1) Prey Good Morning Morgen T-shirt (Men’s Large), with an estimated retail value of nineteen pounds and ninety-nine pence sterling (£19.99);</p>
    <p>One (1) Prey Blue Neon Game Logo Hoodie (Men’s Large), thirty-nine pounds and ninety-nine pence sterling (£39.99);</p>
    <p>One (1) Prey Good Morning Morgan Snapback Cap, with an estimated retail value of nineteen pounds and ninety-nine pence sterling (£19.99).</p>


<p>
    - One (1) PlayStation 4 Pro 1TB, with an estimated retail value of three
    hundred and forty nine pounds sterling (£349);
</p>
<p>
    - One (1) copy of <em>Prey</em> on PlayStation 4 at launch (after 5 May,
    2017), with an estimated retail value of forty four pounds sterling (£44);
</p>
<p>
    - One (1) <em>Prey</em> Good Morning Morgen T-shirt (Men’s Large), with an
    estimated retail value of nineteen pounds and ninety-nine pence sterling
    (£19.99);
</p>
<p>
    - One (1) <em>Prey</em> Blue Neon Game Logo Hoodie (Men’s Large),
    thirty-nine pounds and ninety-nine pence sterling (£39.99); and
</p>
<p>
    - One (1) <em>Prey</em> Good Morning Morgan Snapback Cap, with an estimated
    retail value of nineteen pounds and ninety-nine pence sterling (£19.99).
</p>
<p>
    10. The Prize must be accepted by the Prize Winner as stated and cannot be
    transferred to another person, exchanged for other goods and services or
    redeemed as cash in whole or in part. Any element of a Prize which is not
    accepted will be forfeited, and no compensation will be paid in lieu of
    that element of the Prize. Should a Prize be unavailable at the time that
    the Promoter seeks to acquire the Prize for the Prize Winner, Promoter
    reserves the right, in its sole and absolute discretion, to substitute with
    an alternative of equal or greater value or to substitute a cash
    alternative for any Prize. You agree that the Prize is awarded on an “as
    is” basis and that neither the Promoter nor any of its parents,
    subsidiaries or affiliated companies make any representations or warranties
    of any nature with respect to the Prize. Each Prize Winner in the Promotion
    is solely responsible for any and all applicable taxes (including income
    and withholding taxes on any Prize), regardless of whether the Prize, in
    whole or in part, is used. The actual retail value of any Prize is based on
    available information provided to Promoter and the value of any Prize
    awarded to a Prize Winner must be reported for tax purposes as required by
    law. Prizes may subject to the terms and conditions of a third party prize
    provider which will be made available by such third party; in such
    instance, any complaints or queries relating to the use of the Prize should
    be directed to such third party prize provider.
</p>
<p>
    11. Except for any liability for death or personal injury caused by its
    negligence, fraud or any other liability that cannot be excluded by law,
    the Promoter and its parent companies, subsidiaries, affiliated companies,
    units and divisions, and the current and former officers, directors,
    employees, shareholders, agents, successors and assigns of each of the
    foregoing, and all those acting under the authority of the foregoing or any
    of them (including, but not limited to, advertising and promotional
    agencies and prize suppliers) (collectively, the "Released Parties")
    exclude all liability, claims, actions, injury, loss, damages, liabilities
    and obligations of any kind whatsoever (collectively, “Claims”), whether
    known or unknown, suspected or unsuspected, which entrants have ever had,
    now have, or hereafter can, shall or may have, against the Released Parties
    (or any of them), including, but not limited to, all Claims for personal
    injury, theft, unauthorised access or third party interference arising from
    or related to: (a) the Promotion; and (b) the receipt, ownership, use,
    transfer, sale or other disposition of the Prize, including, but not
    limited to, claims for personal injury, death, and/or property damages.
    Prize Winner shall be solely responsible for any tax liability incurred in
    connection with the Prize.
</p>
<p>
    12. The Promoter accepts no responsibility for any variation of the Prize
    or any aspect of the Prize, due to circumstances outside its reasonable
    control. In any such event, an alternative prize or element of the Prize
    will be arranged.
</p>
<p>
    13. The Promoter is not responsible for any problem or technical
    malfunction of any website or communications network or any late, lost,
    incorrectly submitted, delayed, ineligible, incomplete, corrupted or
    misdirected entry whether due to error, transmission interruption or
    otherwise. The Promoter reserves the right to disqualify any entrant
    submitting an entry which, in the opinion of the Promoter, includes
    objectionable content, including but not limited to profanity, nudity,
    potentially insulting, scandalous, inflammatory or defamatory images or
    language. The Promoter’s decision is final.
</p>
<p>
    <br/>
    14. If, for any reason, the Promotion is not capable of running as planned,
    including, but not limited to, infection by computer virus, bugs,
    tampering, unauthorised intervention, fraud, technical failures or any
    other causes beyond the reasonable control of the Promoter which corrupt or
    affect the administration security, fairness, integrity or proper conduct
    of this Promotion, the Promoter reserves the right in its sole discretion
    to disqualify any individual who tampers with the entry process, and/or to
    cancel, terminate, modify or suspend the Promotion. The Promoter assumes no
    responsibility for any error, omission, interruption, deletion, defect,
    delay in operation or transmission, communications line failure, theft or
    destruction or unauthorised access to, or alteration of, entries beyond its
    reasonable control. The Promoter is not responsible for any problems or
    technical malfunction of any telephone network or lines, computer online
    systems, servers or providers, computer equipment, software, failure of any
    entry to be received by the Promoter on account of technical problems or
    traffic congestion on the Internet or at any website, or any combination
    thereof, including any injury or damage to participant's or any other
    person's computer related to or resulting from participation or downloading
    any materials in this Promotion. CAUTION: ANY ATTEMPT TO DELIBERATELY
    DAMAGE ANY WEBSITE OR THE INFORMATION ON A WEBSITE, OR TO OTHERWISE
    UNDERMINE THE LEGITIMATE OPERATION OF THIS PROMOTION, MAY BE A VIOLATION OF
    CRIMINAL AND CIVIL LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, WHETHER
    SUCCESSFUL OR NOT, THE PROMOTER RESERVES THE RIGHT TO SEEK DAMAGES TO THE
    FULLEST EXTENT PERMITTED BY LAW.
</p>
<p>
    <br/>
    15. The Prize Winner, by acceptance of the Prize, grants to Promoter and
    its parents, subsidiaries, affiliates, designees and assigns the right to
    publicize his or her name, address (city and country of residence),
    photograph, voice, statements and/or other likeness and prize information
    for advertising, promotional and/or trade and/or any other purpose ("Winner
    Information") in any media or format now known or hereafter devised,
    throughout the world, in perpetuity, without limitation and without further
    compensation, consideration, permission or notification, except where
    prohibited by law. The Prize Winner agrees to participate in reasonable
    publicity activities surrounding the Promotion as requested by the
    Promoter.
</p>
<p>
    16. Without limiting the foregoing, the personal information provided by
    entrants in connection with this Promotion will be handled in accordance
    with data protection legislation and in accordance with the Promoter’s
privacy policy, which may be found    <a href="http://www.ziffdavis.com/privacy-policy">here</a>.
</p>
<p>
    17. These Terms and Conditions are governed by the laws of England and
    Wales. The courts located in London, England shall have exclusive
    jurisdiction to hear any dispute or claim arising in association with the
    Promotion or these Terms and Conditions.
</p>
<p>
    18. Although the Promotion may be featured on Twitter and/or Facebook, the
    Promotion is in no way sponsored, endorsed, administered by, or association
    with Twitter or Facebook and you agree that Twitter and Facebook are not
    liable in any way for any claims, damages or losses associated with the
    Promotion.
</p>
<p>
    19. A list of Promotion winners can be made available on request by writing
    to the offices of the Promoter: Ziff Davis International Ltd., 18 Mansell
    Street, London, E1 8AA. A stamped addressed envelope must be included with
    the request.
</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<!-- end terms modal ------------------------------------------------------------------------->
    
    
		<!-- transtar footer -->
        <div class="transtar-footer">
    		<div class="copyright">
        		<img src="img/dashbar.png" alt=""><br>OFFICE OF INTERNAL COMMUNICATIONS<br>TRANSTAR CORPORATION
        	</div>
        	<ul>
        		<li class="social"><a href="https://www.facebook.com/prey" target="_blank"><i class="ion-social-facebook"></i></a></li>
        		<li class="social"><a href="https://twitter.com/preygame" target="_blank"><i class="ion-social-twitter"></i></a></li>
        		<li class="social"><a href="https://www.instagram.com/preygame" target="_blank"><i class="ion-social-instagram-outline"></i></a></li>
        	</ul>
  		</div>
        
        <!-- prey footer -->
        <div class="prey-footer">
    		<a href="https://prey.bethesda.net" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Link']);" ><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
        	<a href="https://bethesda.net/en/dashboard" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Dashboard Link']);" ><img class="bethesda-logo" src="img/bethesda-logo.png" alt="Pre-order"></a>
        	<a href="http://www.arkane-studios.com/uk/index.php" onclick="_gaq.push(['_trackEvent', 'Footer - Arkane Studios Link']);"  target="_blank"><img class="arkane-logo" src="img/arkane-logo.png" alt="Boxshots"></a>
    	</div>
        
    </div>
    <!-- end container -->

    <footer class="ziffdavis-footer">
      <a href="http://www.ziffdavis.com" target="_blank"><img src="img/zd_logo.png" alt="Ziff Davis"></a>
      <div class="text-center">
        <p>&#169; <script type="text/javascript"> document.write(new Date().getFullYear()); </script> Ziff Davis International Ltd</p>
        <ul>
          <li><a href="http://uk.corp.ign.com/#about" target="_blank" title="About Us">About Us</a></li>
          <li><a href="http://uk.corp.ign.com/#contact" target="_blank" title="Contact Us">Contact Us</a></li>
          <li><a href="http://corp.ign.com/feeds.html" target="_blank" title="RSS Feeds">RSS Feeds</a></li>
          <li><a href="http://corp.ign.com/privacy.html" target="_blank" title="Privacy Policy">Privacy Policy</a></li>
          <li><a href="http://corp.ign.com/user-agreement.html" target="_blank" title="Terms of Use">Terms of Use</a></li>
        </ul>
      </div>
    </footer>


    <!-- Back to top -->
	<a href="#0" class="cd-top"><i class="ion-android-arrow-up"></i></a>

<!-- Quiz -->
<script type="text/javascript" src="js/quiz.js?v=2.3"></script>
<script type="text/javascript" src="js/competition.js"></script>

<!-- Twitter share content -->
<script>
	function tweetIt () {
  var phrase = document.getElementById('result').innerText;
  var tweetUrl = 'https://twitter.com/share?text=' +
  	'I am ' +
    encodeURIComponent(phrase) +
	'.' +
    '&url=' +
    'http://uk-microsites.ign.com/';
    
  newwindow=window.open(tweetUrl,'name','height=470,width=550');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>

<!-- Facebook share content -->
<script>

window.fbAsyncInit = function() {
    FB.init({
      appId      : '543961095774853', // App ID
      status     : false, 
      version:   'v2.0',
      cookie     : true, 
      xfbml      : false  // parse XFBML
    });
};

window.addEventListener('load', function(){

    document.getElementById('shareBtn').onclick = function() {
        // text
        var text = document.getElementById('result').innerText;
        // share image
        var image = $('.desc img').attr('src');

    	FB.ui({
            display: 'popup',
            method: 'share',
            title: 'I AM '+text,
            description: 'What job role do you have on Talos I? Take the psychometic test here',
            link: 'http://uk-microsites.ign.com/prey/',
            picture: 'http://uk-microsites.ign.com/prey/'+image,
            href: 'http://uk-microsites.ign.com/prey/'
      }, function(response){

      })
    };

});
</script>

<!-- Scripts -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>

<script type="text/javascript" src="js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>

<?php require_once('partials/footer.html');?>