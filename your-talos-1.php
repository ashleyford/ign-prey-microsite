<?php require_once('partials/header.html');?>

    <!-- preloader -->
    <div id="preloader">
        <div class="pre-container">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <!-- end preloader -->

    <div class="container-fluid">
    
    	<!-- prey header -->
    	<div class="prey-header">
    		<a href="https://prey.bethesda.net" target="_blank"><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
        	<a href="https://prey.bethesda.net/preorder/" target="_blank"><img class="prey-preorder" src="img/prey-preorder.png" alt="Pre-order"></a>
        	<a href="https://prey.bethesda.net/preorder/" target="_blank"><img class="prey-boxshots" src="img/prey-boxshots.png" alt="Boxshots"></a>
        	<img class="prey-coming" src="img/prey-coming.png" alt="Coming 5.5.17">
    	</div>
    
 		<!-- transtar header -->
        <header class="box-header">
            <div class="box-logo">
                <a href="index.php"><img src="img/transtar-logo.png" width="195" alt="TranStar"></a>
            </div>
            
            <div class="date">
        		<div id="time"></div>
			</div>
            
            <div class="location">
        		TRANSTAR HQ<div id="weather"></div>
			</div>
            
            <!-- nav button -->
            <a class="box-primary-nav-trigger" href="#0">
                <span class="box-menu-text">MENU</span><span class="box-menu-icon"></span>
            </a>
        </header>
        <!-- end header -->

        <?php require_once('partials/nav.html'); ?>
    
    	<!-- header -->
    	<div class="top-bar">
        	<div class="article1-top-bar-text">
        	  <h1>THE TALOS I INNOVATION CENTRE</h1>
        	  <p>WHERE DREAMS AND <br>SCIENCE MEET</p>
            </div>
    	</div>
    	<!-- end header -->
	</div>
    
    <div>
        <!-- content -->
        <div class="container main-container clearfix article"> 
            <div class="col-md-12">
               <p>In 2030, Transtar declared the Talos I R&D facility fully operational. In doing so, the conglomerate triggered a seismic shift in private scientific enterprise with this monument to corporate ingenuity.</p> 
                <p>Based in secure orbit around Earth’s moon, the space station is a testament to the vision laid out by the late, great John F Kennedy during his back-to-back presidential terms of the 1960s. Talos I not only delivers on the promise of Kennedy’s pioneering attitude, it embodies the resolute spirit and dogmatic belief the former-president showed in surviving the assassination attempt of 1963. As we know, Kennedy went on to defy critics by taking sole charge of the “Kletka” programme that had previously been a joint project with the Soviet Union. In doing so, he led a nation to greatness in outer space.</p>
                <p>Now, seven decades later, the work carried out aboard Talos I represents the pinnacle of scientific research and engineering endeavour, set against the backdrop of a luxurious home from home for the brightest minds of our own generation.</p>
                <p>To better understand the vital work that takes place across its corporate and consumer-facing R&D divisions, along with the unprecedented progress that Transtar is making in pursuit of human advancement, here is your approved guide to the key areas of Talos I and the many wonders made possible by Transtar.</p>
                <h3 class="bold">PSYCHOTRONICS LAB</h3>
                <p>Alongside its associated testing facilities, the psychotronics lab offers an expansive space for unimpeded research and development in the understanding, manipulation and improvement of the human brain.</p>
                <p>Drawing upon Transtar’s own cutting edge exploration in quantum processes and neurological remapping, the psychotronics lab is helping to redefine what it means to be human. Abilities once deemed the stuff of science fiction are being unlocked thanks to the practical application of pioneering scientific discoveries rooted firmly in tangible fact.</p>
                <h3 class="bold">NEUROMOD DIVISION</h3>
                <p>The Neuromod division is where the possibilities made real by science are packaged into a comfortable and convenient delivery system by the best engineers in the business.</p>
                <p>Neuromods are Transtar’s answer to the logistical issue of how to instantly reconfigure the human brain’s neural pathways, with almost no noticeable lasting pain or side effects. Thanks to the tireless efforts and perseverance of this division’s engineers, Transtar will soon launch its approved range of Neuromods to the consumer market, making quantifiable self-improvement an affordable high-end luxury item.</p>
                <h3 class="bold">HARDWARE SECTOR</h3>
                <p>Responsible for Transtar’s extensive range of manufactured assistants, military-grade personal defence solutions and wearable tech, the hardware sector is the beating mechanical heart of Transtar.</p>
                <p>Amongst the innovations of this exciting sector are the N-CN9 military class blackbox, Sybil 495 science class assistant and engineering-class Kobold 410. The assembly of Transtar’s patented Skill Recorder, which serves as the basis for the creation of Neuromods, is also carried out here. As is the currently-classified psychoscope, which uses laser triangulation of a solid-state light source in order to analyse life-forms, both terrestrial and non-terrestrial.<br><br></p>
                <p>Combined with the self-sustaining onboard power plant, the secure containment space of deep storage, and a suite of ultra-comfortable admin, sales and communications offices, Talos I is a workplace like no other.</p>
                <p>Those considering securing a position aboard the galaxy’s most lavish work place are invited to visit the recruitment pages to take the placement test. You may also be interested in reading more about the work of <a href="the-new-you.html" class="bold">Transtar’s psychotronics lab</a>, and the enigmatic non-terrestrial life-form known as <a href="the-typhon.html" class="bold">Typhon.</a></p>
                <p>We look forward to seeing you aboard Talos I, where the very best of human endeavour and achievement is being harnessed and applied by the Transtar Corporation.</p>
            </div>
        </div>
    
        <!-- transtar footer -->
        <div class="transtar-footer">
            <div class="copyright">
                <img src="img/dashbar.png" alt=""><br>OFFICE OF INTERNAL COMMUNICATIONS<br>TRANSTAR CORPORATION
            </div>
            <ul>
                <li class="social"><a href="https://www.facebook.com/prey" target="_blank"><i class="ion-social-facebook"></i></a></li>
                <li class="social"><a href="https://twitter.com/preygame" target="_blank"><i class="ion-social-twitter"></i></a></li>
                <li class="social"><a href="https://www.instagram.com/preygame" target="_blank"><i class="ion-social-instagram-outline"></i></a></li>
            </ul>
        </div>
        
        <!-- prey footer -->
        <div class="prey-footer">
            <a href="https://prey.bethesda.net" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Link']);" ><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
            <a href="https://bethesda.net/en/dashboard" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Dashboard Link']);" ><img class="bethesda-logo" src="img/bethesda-logo.png" alt="Pre-order"></a>
            <a href="http://www.arkane-studios.com/uk/index.php" onclick="_gaq.push(['_trackEvent', 'Footer - Arkane Studios Link']);"  target="_blank"><img class="arkane-logo" src="img/arkane-logo.png" alt="Boxshots"></a>
        </div>
        
    </div>
    <!-- end container -->

    <footer class="ziffdavis-footer">
      <a href="http://www.ziffdavis.com" target="_blank"><img src="img/zd_logo.png" alt="Ziff Davis"></a>
      <div class="text-center">
        <p>&#169; <script type="text/javascript"> document.write(new Date().getFullYear()); </script> Ziff Davis International Ltd</p>
        <ul>
          <li><a href="http://uk.corp.ign.com/#about" target="_blank" title="About Us">About Us</a></li>
          <li><a href="http://uk.corp.ign.com/#contact" target="_blank" title="Contact Us">Contact Us</a></li>
          <li><a href="http://corp.ign.com/feeds.html" target="_blank" title="RSS Feeds">RSS Feeds</a></li>
          <li><a href="http://corp.ign.com/privacy.html" target="_blank" title="Privacy Policy">Privacy Policy</a></li>
          <li><a href="http://corp.ign.com/user-agreement.html" target="_blank" title="Terms of Use">Terms of Use</a></li>
        </ul>
      </div>
    </footer>

    <!-- Back to top -->
	<a href="#0" class="cd-top"><i class="ion-android-arrow-up"></i></a>

<!-- Scripts -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>

<?php require_once('partials/footer.html');?>