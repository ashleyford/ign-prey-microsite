<?php require_once('partials/header.html');?>

    <!-- preloader -->
    <div id="preloader">
        <div class="pre-container">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <!-- end preloader -->

    <div class="container-fluid">
    
    	<!-- prey header -->
    	<div class="prey-header">
    		<a href="https://prey.bethesda.net" target="_blank"><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
        	<a href="https://prey.bethesda.net/preorder/" target="_blank"><img class="prey-preorder" src="img/prey-preorder.png" alt="Pre-order"></a>
        	<a href="https://prey.bethesda.net/preorder/" target="_blank"><img class="prey-boxshots" src="img/prey-boxshots.png" alt="Boxshots"></a>
        	<img class="prey-coming" src="img/prey-coming.png" alt="Coming 5.5.17">
    	</div>
    
 		<!-- transtar header -->
        <header class="box-header">
            <div class="box-logo">
                <a href="index.php"><img src="img/transtar-logo.png" width="195" alt="TranStar"></a>
            </div>
            
            <div class="date">
        		<div id="time"></div>
			</div>
            
            <div class="location">
        		TRANSTAR HQ<div id="weather"></div>
			</div>
            
            <!-- nav button -->
            <a class="box-primary-nav-trigger" href="#0">
                <span class="box-menu-text">MENU</span><span class="box-menu-icon"></span>
            </a>
        </header>
        <!-- end header -->

      <?php require_once('partials/nav.html'); ?>
    
    	<!-- header -->
    	<div class="top-bar">
        	<div class="article1-top-bar-text">
        	<h1>THE NEW YOU</h1>
        	<p>REDEFINE WHAT IT MEANS <br>
        	  TO BE HUMAN</p>
        	</div>
    	</div>
    	<!-- end header -->
	</div>
    
    <div>
        <!-- content -->
        <div class="container main-container clearfix article"> 
            <div class="col-md-12">
               <h3 class="bold">“The Last Frontier is the human mind and we are its pioneers” - William Yu, TranStar Corp luminary.</h3>
               <p>The Transtar Corporation is dismantling the boundaries of human limitation and establishing a new framework for personal advancement. Thanks to the ceaseless efforts and harmonious collaboration of its psychotronic lab and Neuromod division, Transtar is proud to bring to market its range of ability-enhancing Neural Modifiers.</p> 
               <p>From improving physical constitution and muscle conditioning to boosting a person’s ability to problem-solve and make astute decisions, Transtar’s Neuromods represent a giant leap in the betterment of the human condition. The serum that is delivered by the convenient, hand-held dispenser introduces new genetic material to the end-user’s neurons, making it possible to alter the brain’s physiology. </p>
               <p>The resultant enhancement of the human mind and body is nothing short of revolutionary and we are pleased to provide a few brief examples of what these incredible devices are capable of:</p>
               <p><span class="bold">Toughness and Fortitude:</span> alter the cell structure of muscle tissue to increase general well-being, resilience and increase the user’s natural life span by a quarter of a century.</p>
               <p><span class="bold">Material comprehension and manipulation:</span> enhance your understanding of the physical world and pursue mastery over mechanical circuitry, item fabrication and advanced-level repair work.</p>
               <p><span class="bold">Security system installation and modification:</span> gain knowledge of otherwise unfathomable computerised systems and personal defence solutions.</p>
               <p>Based on Transtar’s unparalleled understanding of quantum coherence in biological processes, Neuromods will change the way the human race defines expectation and attainment.</p>
               <p>For more information on the Neuromods soon to be released to market, and employee guidance on how to talk about approved consumer use with family and friends, contact the Transtar sales and communications division.</p>
               <p>Transtar’s Neuromods are helping redefine what it means to be human and reshaping our expectations for the future.</p>
               <p>Everything you know is about to change.<br><br></p>
            </div>
            <div class="col-md-12">
                <h3 class="bold">//DATA RETRIEVAL FROM TRANSTAR’S LOOKING GLASS INTERNAL SERVERS BEGINS. STRICTLY FOR TRANSTAR EMPLOYEES ONLY. THE FOLLOWING IS PROTECTED UNDER EMPLOYEES’ TS-76C NON-DISCLOSURE AGREEMENT//</h3>
                <p><br>Some of the results outlined here are made possible in conjunction with the prototype psychoscope, which should only be used inside the test facilities of Talos I and under the supervision of specialist psychotronics lab staff.</p>
               <p>Under no circumstances is this information on prototype Neuromods to be shared outside of the Transtar Corporation without express permission from Transtar CEO Alex Yu or his appointed board of directors.</p>
               <p><span class="bold">Superthermal:</span> a proximity-triggered ball of super-charged plasma, this Neuromod is classified for demonstration to approved military partners only.</p>
               <p><span class="bold">Psychokinesis:</span> remote manipulation of objects and circuitry, this full extent of the practical application of this versatile enhancement is still being explored.</p>
               <p><span class="bold">Mimicry:</span> one of the psychotronics lab’s most experimental Neuromods, this scientific marvel has been observed under strict test conditions as giving the user the ability to realign their atomic composition. In short, it’s possible to shape-shift. </p><br>
               <h3 class="bold">// DATA RETRIEVAL FROM TRANSTAR’S LOOKING GLASS INTERNAL SERVERS ENDS //</h3>
            </div>
        </div>
    
        <!-- transtar footer -->
        <div class="transtar-footer">
            <div class="copyright">
                <img src="img/dashbar.png" alt=""><br>OFFICE OF INTERNAL COMMUNICATIONS<br>TRANSTAR CORPORATION
            </div>
            <ul>
                <li class="social"><a href="https://www.facebook.com/prey" target="_blank"><i class="ion-social-facebook"></i></a></li>
                <li class="social"><a href="https://twitter.com/preygame" target="_blank"><i class="ion-social-twitter"></i></a></li>
                <li class="social"><a href="https://www.instagram.com/preygame" target="_blank"><i class="ion-social-instagram-outline"></i></a></li>
            </ul>
        </div>
        
        <!-- prey footer -->
        <div class="prey-footer">
            <a href="https://prey.bethesda.net" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Link']);" ><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
            <a href="https://bethesda.net/en/dashboard" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Dashboard Link']);" ><img class="bethesda-logo" src="img/bethesda-logo.png" alt="Pre-order"></a>
            <a href="http://www.arkane-studios.com/uk/index.php" onclick="_gaq.push(['_trackEvent', 'Footer - Arkane Studios Link']);"  target="_blank"><img class="arkane-logo" src="img/arkane-logo.png" alt="Boxshots"></a>
        </div>
        
    </div>
    <!-- end container -->

    <footer class="ziffdavis-footer">
      <a href="http://www.ziffdavis.com" target="_blank"><img src="img/zd_logo.png" alt="Ziff Davis"></a>
      <div class="text-center">
        <p>&#169; <script type="text/javascript"> document.write(new Date().getFullYear()); </script> Ziff Davis International Ltd</p>
        <ul>
          <li><a href="http://uk.corp.ign.com/#about" target="_blank" title="About Us">About Us</a></li>
          <li><a href="http://uk.corp.ign.com/#contact" target="_blank" title="Contact Us">Contact Us</a></li>
          <li><a href="http://corp.ign.com/feeds.html" target="_blank" title="RSS Feeds">RSS Feeds</a></li>
          <li><a href="http://corp.ign.com/privacy.html" target="_blank" title="Privacy Policy">Privacy Policy</a></li>
          <li><a href="http://corp.ign.com/user-agreement.html" target="_blank" title="Terms of Use">Terms of Use</a></li>
        </ul>
      </div>
    </footer>


    <!-- Back to top -->
	<a href="#0" class="cd-top"><i class="ion-android-arrow-up"></i></a>

<!-- Scripts -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>

<?php require_once('partials/footer.html');?>