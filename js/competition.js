// Define the root namespace if necessary
var JS = window.JS || {};

/**
* Behaviours
* @namespace 
* @requires
*/
JS.comp = {

	config: {
		apiBase: "https://ign.ashleyford.co.uk/api/entry",
		campaignId: "ign_prey",
		submitBtn: "#enter_comp",
		formHolder: "#competition_form",
		responseHolder: "#response_holder"
	},


	init: function() {

		this.formSetup();
	
	},

	formSetup: function(){

		that = this;
		
		$( that.config.submitBtn ).click(function(){
			that.toggleSubmitDisabled();
			that.formSubmit();
		});

	},

	formSubmit: function(){

		var entry = {
			first_name :$( "input[name*='first_name']" ).val(),
			last_name: $( "input[name*='last_name']" ).val(),
			email:$( "input[name*='email']" ).val(),
			campaign: that.config.campaignId,
			type: $( "input[name*='type']" ).val(),
			terms: ($( "input[name*='terms']:checked" ).length > 0) ? 1 : ""
		};

		$.ajax({
		    type: "POST",
		    data :JSON.stringify(entry),
		    url: that.config.apiBase,
		    contentType: "application/json",
			dataType: 'json',
			success: function(response) {
		    	that.formResponse(response);
		    	that.toggleSubmitDisabled();
			}
		});

	},

	toggleSubmitDisabled: function(){

		$( this.config.submitBtn ).prop('disabled', function(i, v) { return !v; });

	},

	formResponse: function(response){

		// populate response message
		$( this.config.responseHolder ).html( response.message );
		// add alert classes - .alert-success .alert-warning .alert-danger
		$( this.config.responseHolder ).removeClass().addClass( 'alert '+response.class );

		if(response.status === true){
			$(this.config.formHolder).hide();
		}

	}
};

// Call It

window.addEventListener('load', function(){
	JS.comp.init();
});