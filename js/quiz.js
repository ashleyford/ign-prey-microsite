// Define the root namespace if necessary
var JS = window.JS || {};

/**
* Behaviours
* @namespace 
* @requires
*/
JS.quiz = {

	config: {
		quizResults: "#results",
		quizQuestion: ".quiz-step",
		quizAnswer: ".quiz-answer",
		numAnswered: 1,
		quizSteps: 9,
		quizQuestionsTotal:10,
		tie: false,
	},

	quizScore: {
		answer0: 0,
		answer1: 0,
		answer2: 0
		},

 	resultOptions: {
     		answer0: {  
     			title: 'MORGAN YU',
        		desc: '<img src="img/morgan.jpg" class="img-responsive" style="margin: auto" />'
        	},
    		answer1:{
    			title: 'SARAH ELAZAR',
        		desc: '<img src="img/sarah.jpg" class="img-responsive" style="margin: auto" />'
        	},
    		answer2: {
    			title: 'MIKHAILA ILYUSHIN',
        		desc: '<img src="img/mikhaila.jpg" class="img-responsive" style="margin: auto" />'
        	}
	},

	init: function() {

		this.loadQuiz();
	
	},

	loadQuiz: function(){

		var that = this;

		$( that.config.quizAnswer ).click(function(){

			var index = $( this ).data('quizindex');
			// add value to correct quiz score
			that.quizScore['answer'+index] = that.quizScore['answer'+index]+1;
			// incement number answered
			that.config.numAnswered = that.config.numAnswered+1;
			//that.config.quizStepsMinusTie = that.config.quizSteps+1;

			if(that.config.numAnswered === that.config.quizSteps){

				if(that.config.quizSteps === that.config.quizQuestionsTotal){
					that.calcScore();
				}else{
					that.tieCheck();	
				}

			}else{

				$('.quiz-step').hide();
				$('.quiz-step.step'+(that.config.numAnswered)).css('display', 'inline');

			}

		});

	},

	tieCheck: function(){

		if(this.quizScore['answer0'] === this.quizScore['answer1'] 
		&& this.quizScore['answer0'] != 0
		&& this.quizScore['answer1'] != 0)
		{
			this.config.tie = true;
			this.tieBreaker(2);
			// console.log('0 and 1 match hide 2');
		}

		if(this.quizScore['answer1'] === this.quizScore['answer2']
		&& this.quizScore['answer1'] != 0
		&& this.quizScore['answer2'] != 0)
		{
			this.config.tie = true;
			this.tieBreaker(0);
			// console.log('1 and 2 match hide 0');
		}

		if(this.quizScore['answer0'] === this.quizScore['answer2']
		&& this.quizScore['answer0'] != 0
		&& this.quizScore['answer2'] != 0)
		{
			this.config.tie = true;
			this.tieBreaker(1);
			// console.log('0 and 2 match hide 1');
		}

		// if it's not a tie
		if(this.config.tie == false){

			this.calcScore();

		}

	},

	tieBreaker: function(hideQuestion){

		this.config.quizSteps = this.config.quizSteps+1;
		$('.quiz').hide();
		$('.quiz-answer').filter('[data-quizindex="'+hideQuestion+'"]').hide();
		$('.tie-break').css('display', 'inline');

	},

	calcScore: function(){

		scores = this.quizScore;
		highestScore = 0;

		// loop through array and find the 
		// biggest number
		$.each( scores, function( index, value ){
			if(value > highestScore){
				// highestScore number
				highestScore = value;
				// highestScore index
				highestIndex = index;
			}
		});
		
		$('#result h1').html(this.resultOptions[highestIndex].title);
		$('#type').val(this.resultOptions[highestIndex].title);
		$('.desc').html(this.resultOptions[highestIndex].desc);

		$(this.config.quizQuestion).hide();
		$(this.config.quizResults).css('display', 'inline');

	}

};

// Call It

window.addEventListener('load', function(){
	JS.quiz.init();
});