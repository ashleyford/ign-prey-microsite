<?php require_once('partials/header.html');?>

    <!-- preloader -->
    <div id="preloader">
        <div class="pre-container">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <!-- end preloader -->

    <div class="container-fluid">
    
    	<!-- prey header -->
    	<div class="prey-header">
    		<a href="https://prey.bethesda.net" target="_blank" onclick="_gaq.push(['_trackEvent', 'Bethesda Link']);"><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
        	<a href="https://prey.bethesda.net/preorder/" onclick="_gaq.push(['_trackEvent', 'Preorder Link']);" target="_blank"><img class="prey-preorder" src="img/prey-preorder.png" alt="Pre-order"></a>
        	<a href="https://prey.bethesda.net/preorder/" onclick="_gaq.push(['_trackEvent', 'Preorder Link']);" target="_blank"><img class="prey-boxshots" src="img/prey-boxshots.png" alt="Boxshots"></a>
        	<img class="prey-coming" src="img/prey-coming.png" alt="Coming 5.5.17">
    	</div>
    
 		<!-- transtar header -->
        <header class="box-header">
            <div class="box-logo">
                <a href="index.php"><img src="img/transtar-logo.png" width="195" alt="TranStar"></a>
            </div>
            
            <div class="date">
        		<div id="time"></div>
			</div>
            
            <div class="location">
        		TRANSTAR HQ<div id="weather"></div>
			</div>
            
            <!-- nav button -->
            <a class="box-primary-nav-trigger" href="#0">
                <span class="box-menu-text">MENU</span><span class="box-menu-icon"></span>
            </a>
        </header>
        <!-- end header -->

        <?php require_once('partials/nav.html'); ?>
        
        <!-- intro video -->
        <section class="box-intro">
            <div class="embed-responsive embed-responsive-16by9">
    			<video id="video1" class="embed-responsive-item" autoplay onended="run()" playsinline muted>
  					<source src="video/transtar-header-flyin.mp4?id=1" type="video/mp4">
  					<img src="img/video-backup.jpg" title="Your browser does not support HTML5 video.">
				</video>

				<video id="video2" class="embed-responsive-item" loop playsinline muted autoplay>
  					<source src="video/transtar-header-loop.mp4?id=1" type="video/mp4">
 					<img src="img/video-backup.jpg" title="Your browser does not support HTML5 video.">
				</video>
        	</div>
            
        </section>
        <!-- end intro video -->
    </div>

    <!-- content blocks -->
    <div class="portfolio-div">
        <div class="portfolio">
            <div class="no-padding portfolio_container">
                
                <!-- block 01 -->
                <div class="col-md-3 col-sm-6">
                    <a href="video/prey-history-low.mp4" onclick="_gaq.push(['_trackEvent', 'Home - Our History Link']);"  class="portfolio_item venobox" data-gall="gallery-videos">
                        <img src="img/block-01.jpg" alt="Our History" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                </div>
                <!-- end block 01 -->

                <!-- block 02 -->
                <div class="col-md-3 col-sm-6">
                    <a href="the-new-you.php" onclick="_gaq.push(['_trackEvent', 'Home - The New You Link']);" class="portfolio_item">
                        <img src="img/block-02.jpg" alt="The New You" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                </div>
                <!-- end block 02 -->

                <!-- block 03 -->
                <div class="col-md-6 col-sm-12">
                    <a href="join.php" class="portfolio_item" onclick="_gaq.push(['_trackEvent', 'Home - Join Link']);">
                        <img src="img/block-03.jpg" alt="Join TranStar Today" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                </div>
                <!-- end block 03 -->

                <!-- block -4 -->
                <div class="col-md-3 col-sm-6">
                        <!--LINK HERE--><a href="#" class="portfolio_item" onclick="_gaq.push(['_trackEvent', 'Home - The Typhon Link']);">
                        <img src="img/block-04.jpg" alt="The Typhon" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                </div>
                <!-- end block 04 -->

                <!-- block 05 -->
                <div class="col-md-3 col-sm-6">
                    <a data-vbtype="youtube" href="http://youtu.be/0bXrrYK23jE" data-autoplay="true" onclick="_gaq.push(['_trackEvent', 'Home - Gallery Video Link']);" class="portfolio_item venobox" data-gall="gallery-videos">
                        <img src="img/block-05.jpg" alt="Video" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                </div>
                <!-- end block 05 -->
                
                <!-- block 06 -->
                <div class="col-md-6 col-sm-12">
                    <a href="img/gallery/1.jpg" class="portfolio_item venobox" onclick="_gaq.push(['_trackEvent', 'Home - Gallery Images Link']);" data-gall="gallery-images">
                        <img src="img/block-06.jpg" alt="Gallery" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                    <a href="img/gallery/2.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
                    <a href="img/gallery/3.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
                    <a href="img/gallery/4.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
                    <a href="img/gallery/5.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
                    <a href="img/gallery/6.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>
                    <a href="img/gallery/8.jpg" class="portfolio_item venobox" data-gall="gallery-images"></a>

                </div>
                <!-- end block 06 -->
 
                <!-- block 07 -->
                <div class="col-md-3 col-sm-6">
                    <a href="https://prey.bethesda.net" target="_blank" onclick="_gaq.push(['_trackEvent', 'Home - Bethesda Link']);" class="portfolio_item">
                        <img src="img/block-07.jpg" alt="Prey" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                </div>
                <!-- end block 07 -->

                <!-- block 08 -->
                <div class="col-md-3 col-sm-6">
                    <a data-vbtype="youtube" href="http://youtu.be/kQpE7OFzajs" data-autoplay="true" onclick="_gaq.push(['_trackEvent', 'Home - Video Gallery Link']);" class="portfolio_item venobox" data-gall="gallery-videos">
                        <img src="img/block-08.jpg" alt="Video" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                </div>
                <!-- end block 08 -->

                <!-- block 09 -->
                <div class="col-md-3 col-sm-6">
                    <a data-vbtype="youtube" href="http://youtu.be/NOJt6ibOc-8" data-autoplay="true" onclick="_gaq.push(['_trackEvent', 'Home - Video Gallery Link']);" class="portfolio_item venobox" data-gall="gallery-videos">
                        <img src="img/block-09.jpg" alt="Video" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                </div>
                <!-- end block 09 -->
                
                 <!-- block 10 -->
                <div class="col-md-3 col-sm-6">
                    <a href="your-talos-1.php" class="portfolio_item" onclick="_gaq.push(['_trackEvent', 'Home - Your Talos 1 Link']);">
                        <img src="img/block-10.jpg" alt="Your Talos I" class="img-responsive" />
                        <div class="portfolio_item_hover">
                        </div>
                    </a>
                </div>
                <!-- end block 10 -->
                
            </div>
            <!-- end portfolio_container -->
        </div>
        <!-- end content blocks -->
        
        <!-- transtar footer -->
        <div class="transtar-footer">
            <div class="copyright">
                <img src="img/dashbar.png" alt=""><br>OFFICE OF INTERNAL COMMUNICATIONS<br>TRANSTAR CORPORATION
            </div>
            <ul>
                <li class="social"><a href="https://www.facebook.com/prey" target="_blank"><i class="ion-social-facebook"></i></a></li>
                <li class="social"><a href="https://twitter.com/preygame" target="_blank"><i class="ion-social-twitter"></i></a></li>
                <li class="social"><a href="https://www.instagram.com/preygame" target="_blank"><i class="ion-social-instagram-outline"></i></a></li>
            </ul>
        </div>
        
        <!-- prey footer -->
        <div class="prey-footer">
            <a href="https://prey.bethesda.net" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Link']);" ><img class="prey-logo" src="img/prey-logo.png" alt="Prey"></a>
            <a href="https://bethesda.net/en/dashboard" target="_blank" onclick="_gaq.push(['_trackEvent', 'Footer - Bethesda Dashboard Link']);" ><img class="bethesda-logo" src="img/bethesda-logo.png" alt="Pre-order"></a>
            <a href="http://www.arkane-studios.com/uk/index.php" onclick="_gaq.push(['_trackEvent', 'Footer - Arkane Studios Link']);"  target="_blank"><img class="arkane-logo" src="img/arkane-logo.png" alt="Boxshots"></a>
        </div>
        
    </div>
    <!-- end container -->

    <footer class="ziffdavis-footer">
      <a href="http://www.ziffdavis.com" target="_blank"><img src="img/zd_logo.png" alt="Ziff Davis"></a>
      <div class="text-center">
        <p>&#169; <script type="text/javascript"> document.write(new Date().getFullYear()); </script> Ziff Davis International Ltd</p>
        <ul>
          <li><a href="http://uk.corp.ign.com/#about" target="_blank" title="About Us">About Us</a></li>
          <li><a href="http://uk.corp.ign.com/#contact" target="_blank" title="Contact Us">Contact Us</a></li>
          <li><a href="http://corp.ign.com/feeds.html" target="_blank" title="RSS Feeds">RSS Feeds</a></li>
          <li><a href="http://corp.ign.com/privacy.html" target="_blank" title="Privacy Policy">Privacy Policy</a></li>
          <li><a href="http://corp.ign.com/user-agreement.html" target="_blank" title="Terms of Use">Terms of Use</a></li>
        </ul>
      </div>
    </footer>


	<!-- Back to top -->
	<a href="#0" class="cd-top"><i class="ion-android-arrow-up"></i></a>

<!-- Scripts -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>
    
<!-- Video -->
<script> 
$( "#video2" ).hide();
function run(){
   $( "#video1" ).hide();
    $( "#video2" ).show();
    document.getElementById("video2").play();
   };
</script>   
    

<?php require_once('partials/footer.html');?>
